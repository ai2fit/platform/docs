![Build Status](https://gitlab.com/ai2fit/platform/docs/badges/main/pipeline.svg)
![Build Status](https://gitlab.com/ai2fit/platform/docs/-/badges/release.svg)
[![Built with Material for MkDocs](https://img.shields.io/badge/Material_for_MkDocs-526CFE?style=for-the-badge&logo=MaterialForMkDocs&logoColor=white)](https://squidfunk.github.io/mkdocs-material/)
---

# Docs
This project contains the documentation website for [docs.ai2fit.org](https://docs.ai2fit.org/)

## Usage
## Project layout
```
.
├─ docs/
│  ├─ assets/      # Media files images, videos and other asset files.
│  ├─ blog/
│  │  ├─ posts/    # Each post in its own .md file 
│  │  └─ index.md  # The entry point to the blog
│  └─ index.md     # The documentation homepage
├─ overrides/
│  └─ main.html    # Override (and extend) template blocks
└─ mkdocs.yml      # The configuration file.
```
### Add content

### Building locally

To work locally with this project, you'll have to follow the steps below:
1. Fork, clone or download this project
2. [Install](https://squidfunk.github.io/mkdocs-material/getting-started/) Material for MkDocs
3. If you're running Material for MkDocs from within Docker, use:

   For Unix, Powershell:
   ```
   docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
   ```
   
   For Windows:
   ```
   docker run --rm -it -p 8000:8000 -v "%cd%":/docs squidfunk/mkdocs-material
   ```

   now, your site can be accessed under `localhost:8000`
4. Add or update content and the website will get updated responsively
5. Generate the website: `mkdocs build` (optional)

Read more at Material for MkDocs [documentation](https://squidfunk.github.io/mkdocs-material/reference/).
