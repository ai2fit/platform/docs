---
title: Installation
---

### Improved tooltips

<!-- md:version 9.5.0 -->
<!-- md:module core -->
<!-- md:operator core:retrieve -->

When improved tooltips are enabled, Material for MkDocs replaces the browser's
rendering logic for `title` attribute with beautiful little tooltips.
Add the following lines to `mkdocs.yml`:

``` yaml
theme:
  features:
    - content.tooltips
```

!!! note "Phasellus posuere in sem ut cursus"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

If you're running Material for MkDocs from within Docker, use:

=== "Unix, Powershell"

    ```
    docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
    ```

=== "Windows"

    ```
    docker run --rm -it -p 8000:8000 -v "%cd%":/docs squidfunk/mkdocs-material
    ```
