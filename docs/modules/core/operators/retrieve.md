---
template: operator.html
description: An operator that reads an object from the data repository
tags:
  - Load	
  - Import	
  - Read	
  - Datasets	
  - Examples	
  - Example Set	
  - Table	
  - Repository	
  - Data Access
---
# Retrieve

This operator can be used to access the repositories. It should replace all file
access, since it provides full meta data processing, which eases the usage of AI2FIT a lot. In contrast to
accessing a raw file, it will provide the complete meta data of the data, so that all meta data transformations
are possible. 

The single parameter "repository_entry" references an entry in the repository which will be returned
as the output of this operator. Repository locations are resolved relative to the repository folder containing
the current process. Folders in the repository are separated by a forward slash (/), a ".." references
the parent folder. A leading forward slash references the root folder of the repository containing the current
process. A leading double forward slash is interpreted as an absolute path starting with the name of a
repository. 

- `MyData` looks up an entry "MyData" in the same folder as the current process.
- `../input/MyData` looks up an entry "MyData" located in a folder "input" next to
    the folder containing the current process.
- `/data/Model` looks up an entry "Model" in a top-level folder "data" in the
    repository holding the current process
- `//Samples/data/Iris` looks up the Iris data set in the "Samples" repository.
