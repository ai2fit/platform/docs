# Conventions

### <!-- md:version --> – Version { data-toc-label="Version" }

The tag symbol in conjunction with a version number denotes when a specific
feature or behavior was added. Make sure you're at least on this version
if you want to use it.

### <!-- md:module --> – Module { data-toc-label="Module" }

:construction: Description for modules ... :construction:

### <!-- md:operator --> – Operator { data-toc-label="Operator" }

:construction: Description for operators ... :construction:
