# Changelog

## AI2FIT Platform

### 9.5.1 <small>June 16, 2024</small> { id="9.5.1" }

- Updated translations

### 9.5.0 <small>June 6, 2024</small> { id="9.5.0" }

- Fixed #7232: Tab switches on scroll when linking tabs (9.5.0 regression)
