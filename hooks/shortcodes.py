from __future__ import annotations

import posixpath
import re

from mkdocs.config.defaults import MkDocsConfig
from mkdocs.structure.files import File, Files
from mkdocs.structure.pages import Page
from re import Match


def on_page_markdown(markdown: str, *, page: Page, config: MkDocsConfig, files: Files):
    # Replace callback
    def replace(match: Match):
        type, args = match.groups()
        args = args.strip()
        if type == "version":
            return _badge_for_version(args, page, files)
        elif type == "module":
            return _badge_for_module(args, page, files)
        elif type == "operator":
            return _badge_for_operator(args, page, files)

        # Otherwise, raise an error
        raise RuntimeError(f"Unknown shortcode: {type}")

    # Find and replace all external asset URLs in current page
    return re.sub(
        r"<!-- md:(\w+)(.*?) -->",
        replace, markdown, flags=re.I | re.M
    )


# Create badge for version
def _badge_for_version(text: str, page: Page, files: Files):
    spec = text
    path = f"changelog/index.md#{spec}"

    # Return badge
    icon = "material-tag-outline"
    href = _resolve_path("conventions.md#version", page, files)
    return _badge(
        icon=f"[:{icon}:]({href} 'Minimum version')",
        text=f"[{text}]({_resolve_path(path, page, files)})" if spec else ""
    )


# Create badge for module
def _badge_for_module(text: str, page: Page, files: Files):
    icon = "material-package-variant"
    href = _resolve_path("conventions.md#module", page, files)
    return _badge(
        icon=f"[:{icon}:]({href} 'Module')",
        text=f"[{text.capitalize()}]({_resolve_path('modules/' + text + '/index.md', page, files)})" if text else ""
    )

# Create badge for operator
def _badge_for_operator(text: str, page: Page, files: Files):
    icon = "material-toy-brick-search"
    href = _resolve_path("conventions.md#operator", page, files)
    if text:
        module, operator = text.split(":", 2)
        path = f"modules/{module}/operators/{operator}.md"
        return _badge(
            icon=f"[:{icon}:]({href} 'Operator is used')",
            text=f"[{text}]({_resolve_path(path, page, files)})"
        )
    else:
        return _badge(
            icon=f"[:{icon}:]({href} 'Operator is used')",
            text=""
        )

# Resolve path of file relative to given page - the posixpath always includes
# one additional level of `..` which we need to remove
def _resolve_path(path: str, page: Page, files: Files):
    path, anchor, *_ = f"{path}#".split("#")
    file = files.get_file_from_path(path)
    if file is None:
        raise RuntimeError(f"Unknown file {path} referenced in {page.file.src_uri}")
    path = _resolve(file, page)
    return "#".join([path, anchor]) if anchor else path


# Resolve path of file relative to given page - the posixpath always includes
# one additional level of `..` which we need to remove
def _resolve(file: File, page: Page):
    path = posixpath.relpath(file.src_uri, page.file.src_uri)
    return posixpath.sep.join(path.split(posixpath.sep)[1:])


# Create badge
def _badge(icon: str, text: str = "", type: str = ""):
    classes = f"mdx-badge mdx-badge--{type}" if type else "mdx-badge"
    return "".join([
        f"<span class=\"{classes}\">",
        *([f"<span class=\"mdx-badge__icon\">{icon}</span>"] if icon else []),
        *([f"<span class=\"mdx-badge__text\">{text}</span>"] if text else []),
        f"</span>",
    ])
